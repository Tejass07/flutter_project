import 'package:flutter/material.dart';
import 'nextpage2.dart';
class NextPageState extends StatelessWidget{
  const NextPageState({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar( 
        title: const Text("NextPage"),
        centerTitle: true,
        backgroundColor: Colors.blue,

      ),
      body: Center(
        child:ElevatedButton(onPressed:(){

            Navigator.push(context, MaterialPageRoute(builder: (context) => NewState(),));

        },child:Text( "Next",) ,),
    ),
    );
  }
}