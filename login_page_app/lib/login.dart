
import 'package:flutter/material.dart';
import 'nextpage.dart';

class LoginPage extends StatefulWidget{
      const LoginPage({super.key});

      @override 
      State createState()=> _LoginPage();
}

class _LoginPage extends State<LoginPage>{

      TextEditingController userNameTextEditingController = TextEditingController();
      TextEditingController passwordTextEditingController = TextEditingController();
      
      GlobalKey<FormState> _formKey = GlobalKey<FormState>();
      



      @override
      Widget build(BuildContext context){
        return Scaffold( 
          appBar: AppBar( 
            title: const Text("lOGIN PAGE"),
            centerTitle: true,
          ),
          body: Padding( 
            padding: const EdgeInsets.all(8.0),
            child: Form( 
              key:_formKey,
              child: Column( 
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [ 
                  const SizedBox( 
                    height: 20,
                  ),
                  Image.network("https://tse1.mm.bing.net/th?id=OIP.HVNXUHVRNOsTFaJnPbDICgHaH_&pid=Api&rs=1&c=1&qlt=95&w=114&h=123"
                 , width: 90, height: 90,),
                 const SizedBox( 
                  height: 20,
                 ),

                 TextFormField( 
                  controller: userNameTextEditingController,
                  decoration: InputDecoration( 
                    hintText: "ENTER USER NAME",
                    label: const Text("username"),
                    border: OutlineInputBorder( 
                      borderRadius: BorderRadius.circular(20),
                    ),
                    prefixIcon: const Icon( 
                      Icons.person,
                    ),
                  ),

                  validator: (value){
                    print("in USERNAME VALIDATOR");
                    if(value == null || value.isEmpty){
                      return "ENTER USERNAME";
                    }else{
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                 ),
                 const SizedBox( 
                  height: 20,
                 ),  

                 TextFormField( 
                  controller: passwordTextEditingController,
                  
                  obscureText: true,
                  obscuringCharacter: "*",
                  decoration: InputDecoration( 
                    hintText:"ENTER PASSWORD",
                    border:OutlineInputBorder( 
                      borderRadius: BorderRadius.circular(20),
                    ),
                    prefixIcon:const Icon( 
                      Icons.lock,
                    ),
                    suffixIcon: const Icon( 
                      Icons.remove_red_eye_outlined,
                    ),
                  ),

                  validator: (value){ 
                    print("IN PASSWORD VALIDATOR");

                    if(value == null || value.isEmpty){
                      return "please enter password";
                    }else{
                      return null;
                    }
                  },
                 ),

                    const SizedBox( 
                      height: 20,
                    ),               

                    ElevatedButton(
                      onPressed:(){ 
                        bool loginValidated = _formKey.currentState!.validate();
                        if(loginValidated){
                          ScaffoldMessenger.of(context).showSnackBar( 
                            const SnackBar(content: Text("LOGIN SUCCESS")),
                          );
                        }else{
                          ScaffoldMessenger.of(context).showSnackBar( 
                            const SnackBar(content: 
                            Text("loginFailed")),
                          );
                        }
                        Navigator.push(context, MaterialPageRoute(builder:(context) => NextPageState(),));
                      } ,
                       child: const Text("Login"),
                ),
                ],
              ),
            ),
          ),
        );
      }
}