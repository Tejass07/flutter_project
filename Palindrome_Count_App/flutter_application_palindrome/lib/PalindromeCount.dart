import "package:flutter/material.dart";

class PalindromeCount extends StatefulWidget {
  const PalindromeCount({super.key});

  @override
  State<PalindromeCount> createState() => _PalindromeCountState();
}

class _PalindromeCountState extends State<PalindromeCount> {
  int count = 0;
  void countPalindrome() {
    count = 0;
    List<int> numberList = [1, 225, 777, 121, 111, 234, 999];
    for (int i = 0; i < numberList.length; i++) {
      int temp = numberList[i].abs();
      int reverseNum = 0;
      while (temp != 0) {
        reverseNum = reverseNum * 10 + temp % 10;
        temp = temp ~/ 10;
      }
      if (reverseNum == numberList[i].abs()) {
        count++;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("palindrome count of" + "1,225,777,121,111,234,999 "),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                countPalindrome();
                setState(() {});
              },
              child: const Text("check palindorme"),
            ),
            const SizedBox(
              height: 20,
            ),
            Text("$count Numbers are palindrome")
          ],
        ),
      ),
    );
  }
}
