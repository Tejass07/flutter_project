
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class Zomato{
  int? orderNo;
  final String custName;
  final String hotelName;
  final String food;
  final double bill;

  Zomato({
    this.orderNo,
    required this.custName,
    required this.hotelName,
    required this.food,
    required this.bill,
  });

  Map<String,dynamic> ZomatoMap(){ 
    return { 
      'custName':custName,
      'hotelName': hotelName,
      'food' : food,
      'bill' : bill,
    };
  }

  @override
  String toString(){ 
    return '{orderNo:$orderNo , custName:$custName , hotelName:$hotelName , food:$food, bill:$bill}';
  }
}
  
Future<void> insertOrderData(Zomato obj) async{ 
      final localDB = await database;

      await localDB.insert( 
        "OrderFood",
        obj.ZomatoMap(),
        conflictAlgorithm:ConflictAlgorithm.replace,
      );

  }

Future<List<Zomato>> getOrderData() async {
      final localDB = await database;

      List<Map<String,dynamic>> orderMap = await localDB.query("OrderFood");

      return List.generate(orderMap.length, (i){ 
        return Zomato( 
          orderNo: orderMap[i]['orderNo'],
          custName: orderMap[i]['custName'],
          hotelName: orderMap[i]['hotelName'],
          food:orderMap[i]['food'],
          bill: orderMap[i]['bill'],
        );
      } );
}





dynamic database;

void main() async {
    WidgetsFlutterBinding.ensureInitialized();

    database = openDatabase( 
      join(await getDatabasesPath(), "abc.db"),
      version: 1,
      onCreate: (db,version) async{
        await db.execute( 
          '''
          CREATE TABLE orderfood( 
            orderNo INTEGER PRIMARY KEY,
            custName TEXT,
            hotelName TEXT,
            food TEXT,
            bill REAL
          )
          '''
        );
      }
    );

//insert

Zomato order1 = Zomato(custName: "Ashish", hotelName:"deccan", food:"Panner", bill: 530.50);

insertOrderData(order1);

print (await getOrderData());

}
