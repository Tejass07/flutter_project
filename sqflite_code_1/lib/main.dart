import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

dynamic database;

class Player{ 
  final String name ;
  final int jerNo ;
  final int runs;
  final double avg;

  Player({ 
    required this.name,
    required this.jerNo,
    required this.runs,
    required this.avg,
  });

  Map<String,dynamic> playerMap(){ 
    return{ 
      'name': name,
       'jerNo': jerNo ,
       'runs': runs,
       'avg': avg,
    };
  } 
  @override
  String toString(){
    return'{name:$name, jerNo:$jerNo , runs:$runs , avg:$avg}';
  }
}

Future insertPlayerData(Player obj) async{ 
        final localDB = await database;
        
        await localDB.insert( 
          "Player",
          obj.playerMap(),
          conflictAlgorithm: ConflictAlgorithm.replace
        ); 
}  


Future<List<Player>> getPlayerData() async{ 
  final localDB = await database;
  List<Map<String , dynamic>> listPlayers = await localDB.query("player");

  return List.generate(listPlayers.length, (i) {
  return Player( 
    name:listPlayers[i]['name'],
    jerNo:listPlayers[i]['jerNo'],
    runs:listPlayers[i]['runs'],
    avg:listPlayers[i]['avg'],
  );
});

}


void main() async {

   //WidgetsFlutterBinding.ensureInitialized();
  database = await openDatabase( 
    join(await getDatabasesPath(), "playerDB.db"),
    version: 1,
    onCreate: (db, version) async { 
      await db.execute( 
        ''' CREATE TABLE player( 
          name TEXT,
          jerNo INTEGER PRIMARY KEY,
          runs INT,
          avg REAL
         )'''
      );
    },
  );

  Player batsman1 = Player( 
    name: "Virat Kohli",
    jerNo: 18,
    runs:15000,
    avg:50.33,
  );
  
  await insertPlayerData(batsman1);

   Player batsman2 = Player( 
    name: "Rohit Sharma",
    jerNo: 45,
    runs:16000,
    avg:60.33,
  );
    await insertPlayerData(batsman2);
   
    Player batsman3 = Player( 
    name: "MSD",
    jerNo: 7,
    runs:13000,
    avg:40.33,
  );
   await insertPlayerData(batsman3);

  print(await getPlayerData()); 


  
}

