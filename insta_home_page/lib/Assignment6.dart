//import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class Assignment6 extends StatefulWidget {
  @override
  State<Assignment6> createState() => _Assignment6State();
}

class _Assignment6State extends State<Assignment6> {
  bool _isPost1liked = false;
  // bool _isPost2liked = false;
  // bool _isPost3liked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            "Instagram",
            style: TextStyle(
              fontStyle: FontStyle.italic,
              color: Colors.black,
              fontSize: 30,
            ),
          ),
          actions: const [
            Icon(
              Icons.favorite_rounded,
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Image.network(
                      "https://tse1.mm.bing.net/th?id=OIP.rvSWtRd_oPRTwDoTCmkP5gHaE8&pid=Api&rs=1&c=1&qlt=95&w=154&h=102",
                      width: double.infinity,
                    ),
                  ),
                  Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          setState(() {
                            _isPost1liked = !_isPost1liked;
                          });
                        },
                        icon: _isPost1liked
                            ? const Icon(
                                Icons.favorite_rounded,
                                color: Colors.red,
                              )
                            : const Icon(
                                Icons.favorite_outline_rounded,
                              ),
                      ),
                      IconButton( 
                        onPressed: (){},
                        icon:Icon(Icons.comment_outlined),
                           ),
                           IconButton(
                            onPressed: (){},
                             icon: const Icon( 
                              Icons.send,
                             ),
                           ),
                    ],
                  )
                ],
              )
            ],
          ),
        ));
  }
}
