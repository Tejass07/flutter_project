
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
class ViewEmployee extends StatefulWidget{
      const ViewEmployee({super.key});

      @override  
      State createState() => _ViewEmployee();

}

class _ViewEmployee extends State {
  var empData = [];
  @override  
  Widget build (BuildContext context){
    return Scaffold( 
      appBar: AppBar( 
        title: const Text("API BINDING"),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton( 
        onPressed: getEmployeeData, ),

        body: ListView.builder(
          itemCount: empData.length,
          itemBuilder:(context,index){ 
            return Container( 
              margin: EdgeInsets.all(6.0),
              child: Row(  
                children: [ 
                  Text("${empData[index]['employee_name']}"),

                  const SizedBox(width: 20),

                  Text("${empData[index]['employee_salary']}")
                ],
              ),
             );
          } ),
     );
  }
  void getEmployeeData()async{ 
    Uri url  = Uri.parse("https://dummy.restapiexample.com/api/v1/employees");
    http.Response response = await http.get(url);
    log(response.body);

    var responseData = json.decode(response.body);

    setState((){ 
      empData = responseData['data'];
    });
  }

  
}