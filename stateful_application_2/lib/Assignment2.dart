
import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget{
      const Assignment2({super.key});

      @override
      State<Assignment2> createState()=> _Assignment2State();
}

class _Assignment2State extends State<Assignment2>{
   bool color1=false;
        bool color2=false;
  @override
  Widget build(BuildContext context){
        
        return Scaffold( 
          appBar: AppBar( 
            title: const Text("COLOR BOX"),
            centerTitle: true,
          backgroundColor: Colors.lightBlueAccent,  
          ),
          body: Center(
            child:Row( 
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
            children:[
              
             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [ 
                
                Container( 
                  height: 100,
                  width: 100,
                  color:color1? Colors.blue :Colors.red,
                 ),
                 const SizedBox( 
                  height: 20,
                 ),
                 ElevatedButton(
                  onPressed:(){
                  setState(() {
                   color1 = true;
                  });
                  },
                  child:const Text("BUTTON1"),
                  ),
              ],
             ),
              Column( 
                mainAxisAlignment: MainAxisAlignment.center,
                children: [ 
                 
                  Container( 
                    height:100,
                    width:100,
                    color:color2? Colors.green : Colors.pink,
                  ),
                const SizedBox( 
                  height:20,
                ),
                ElevatedButton(
                  onPressed: (){
                    setState(() {
                      color2=true;
                    });
                  },
                  child: Text("BUTTON2"),
                ),
                ],
              )  
              ],
              

              )
            )
            );
  }
  

}