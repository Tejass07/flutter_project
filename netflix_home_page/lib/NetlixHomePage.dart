
import 'package:flutter/material.dart';

class NetflixHomePage extends StatefulWidget{
      const NetflixHomePage({super.key});

      @override
      State<NetflixHomePage> createState() => _NetflixHomePageState();
}

class _NetflixHomePageState extends State<NetflixHomePage>{
      @override

      Widget build(BuildContext context){
            return Scaffold( 
              appBar: AppBar( 
                title: const Text("NetflixHomePage"),
                backgroundColor: Colors.red,
              ),
              body: SingleChildScrollView( 
                scrollDirection: Axis.vertical,
                child:Column(
                    children:[
                    const SizedBox(
                      height:30
                     ), 
                  
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                  child: Row(
                  
                  children: [
                    
                    const Text("movies"),
                    SizedBox( 
                      height:400,
                      width: 400,
                      child:Image.network("https://mir-s3-cdn-cf.behance.net/project_modules/1400/e5865358516595.59ffa0a2671f5.jpg"),
                    ), 
                    SizedBox( 
                      height: 400,
                      width: 400,
                      child: Image.network("https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/c358f738155793.57575971b1c09.jpg"),
                    ),
                    SizedBox( 
                      height:400,
                      width: 400,
                      child: Image.network("https://mir-s3-cdn-cf.behance.net/project_modules/1400/62332132039857.566bcebd67c82.jpg"),  
                    ),

                  ],
                  ),
                  ),
                  SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child:Row(
                    children: [ 
                      SizedBox( 
                        height:180,
                        width:180,
                        child:Image.network("https://assetscdn1.paytm.com/images/catalog/product/H/HO/HOMSHERLOCK-HOLHK-P63024784A1CC1B/1563111214645_0..jpg"),
                      ),
                      SizedBox(
                        height:180,
                        width:180,
                        child:Image.network("https://dnm.nflximg.net/api/v6/2DuQlx0fM4wd1nzqm5BFBi6ILa8/AAAAQeIeKt7LlqIJPKrT4aQijclj7K43xRSU3dQXNESNdNbnnJbT6LLWVRT9srUUbHbOo-iOH-8v3o16pUDMQ6tCgNGlkvfwvDOprROIZpQ2rgHtop9rHvbYlvzavMmUSGBCXjynJ80dn4nqZzZmzIUJMQpS.jpg?r=943"),
                       ),
                       SizedBox(
                        height:180,
                        width:180,
                        child:Image.network("https://www.tallengestore.com/cdn/shop/products/PeakyBlinders-NetflixTVShow-ArtPoster_125897c4-6348-41e8-b195-d203700ebcca.jpg?v=1619864555"),
                       ),
                    ],
                   ),
                  ),
                  SingleChildScrollView( 
                  scrollDirection : Axis.horizontal,
                  child:Row(
                    children: [ 
                      SizedBox( 
                        height: 180,
                        width:180,
                        child:Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZ5Cq8kozpWIaq5Aohw4rjKkh_eE7nUkDV5zcHClQaYw&s"),
                      ),
                      SizedBox( 
                        height:180,
                        width:180,
                        child:Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZ5Cq8kozpWIaq5Aohw4rjKkh_eE7nUkDV5zcHClQaYw&s"),
                      ),
                      SizedBox( 
                        height:180,
                        width:180,
                        child:Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZ5Cq8kozpWIaq5Aohw4rjKkh_eE7nUkDV5zcHClQaYw&s"),
                      ),
                    ],
                  ),
                ), 
                ],
                )
              ) 
            
            );
      }
}