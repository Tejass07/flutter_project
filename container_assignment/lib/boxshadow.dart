import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


    @override
    Widget build(BuildContext context){
      return Scaffold( 
        appBar: AppBar( 
          title: const Text("CONTAINER STYLISH"
          ),
        ),
        body: Container( 
          height: 200,
          width: 200,
          
          decoration: BoxDecoration( 
              color:Colors.amber,
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              border: Border.all(color:Colors.blue,width: 5),
              boxShadow: const [ 
                BoxShadow( 
                  color: Colors.purple,offset: Offset(30,30),blurRadius: 8),
                BoxShadow( 
                  color: Colors.red,offset: Offset(20,30),blurRadius: 8),
                BoxShadow( 
                  color: Colors.green,offset: Offset(10,30),blurRadius: 8),
                
              ]
        ),
      ),
      );
    } 
}