import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


    @override
    Widget build(BuildContext context){
      return Scaffold( 
        appBar: AppBar( 
          title: const Text("CONTAINER STYLISH"
          ),
        ),
        body: Container( 
          height: 200,
          width: 200,
          
          decoration: BoxDecoration( 
              color:Colors.amber,
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              border: Border.all(color:Colors.blue,width: 5),
              gradient: const LinearGradient( 
                stops: [0.2,0.6,0.3,0.7],
                colors: [Colors.red, Colors.green,Colors.blue, Colors.purple],
              ),
        ),
      ),
      );
    } 
}