import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


    @override
    Widget build(BuildContext context){
      return Scaffold( 
        appBar: AppBar( 
          title: const Text("CONTAINER STYLISH"
          ),
        ),
        body: Container( 
          height: 200,
          width: 200,
          color: Colors.blue,
          margin : const EdgeInsets.only(left:10,right:10,top:10,bottom:10),
        ),
      );
    } 
}