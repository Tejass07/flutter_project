
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Assignment3 extends StatefulWidget{
      const Assignment3({super.key});

      @override
      State<Assignment3> createState() => _Assignment3State();
}

class _Assignment3State extends State<Assignment3>{

  int? selectedIndex = 0;
  final List<String> imageList = [
  "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJQA9AMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAABAIDBQEGB//EAD0QAAICAQIFAgMECAQGAwAAAAECAAMRBCEFEjFBURNhIoGRBhQycSNCVJKhscHRFTNSgjRicqLh8BYkQ//EABoBAAMBAQEBAAAAAAAAAAAAAAACAwEEBQb/xAAoEQACAgEEAgEEAgMAAAAAAAAAAQIDEQQSEyExQVEFFCJhFTJCUoH/2gAMAwEAAhEDEQA/APj/ACWV7EEQCk9t5pWpRaD6L2j2ZesStDVHYg46iVTLzq2P5JsOdF+HdR9ZZXRVyczP0/VAycyhXXAKsQZPmRfiGQf9ONprNi1nJdzVkfiIPvD0A/QIT3kGZLFG2/eFTMmTUAfzi4ZTKbwym3R2JggfxErww2J+U0fvKWACyjf/AFA4MpspQqW3A9zmMsk51r/ArqBrOMDBG8nWEzixgoz1xK06dZJRznH9IwqeBhggPImeQjqesQuQqSPBxNAD0hh0yPcbyBWt2/RjruMxUik47hLT5FgOO28f1GmNRV1GVYcwMp9P03Bx7yw6lmq9E7oDlfI9posUkmpC/qG18HbtiX6YFX6dD/CFNBAV2GF5tzL+QJ8Q7nt4gbCLTyw1FALBl35hkRUlTSEKnPma+p0noqrcxwFB+X95mXJliG232mJlba3HyLocfKW2L8IaV8hWwgy9Bmo5jEEvQm4+KRHXaNW17Zi4HxYmojKOGW8vwDPWVS6w7YlYGYwMrcbyD9sS5pxVy0BWdrQLgmRYFmO20aalEryWyfHiL7scZ67YmGtYG9HWKkVzjZubaLam02gny2TJi0jKjY8uAPHvIMmExFwU3fjtRSte28IMtmekIZF/4FdrJ+s2fzkmtZzlwCfI2nSocZXadrqL7BSW8CGDfy8AKg3xAHPeTKsMDmb5zlaYfH842yJgcmJhSMcorr5So7N3lqJynIEr9M5zGkIFYz+LHmDLQj8leoQEZI3i7oy9jvG3Zn2GJZUCy7gHEzOBnWpPApRUHXB/lGaNOqMSQMgZEvNHKhZRtKqk5nwSYbh1Xtx12dtzqOibcuNpUdDYqZX/AGx5WFLH4cA9Z264MGqUbHpDso64tZl5MxUDj4g3ONsEyyrRM7ghY7RovXtL45cDmbHadqsYFvT6DOMwyJw4w5FVz1iv0By4YYOR9JVRpX+JjkIg5jnxIvWBYCTk56zUqsS/RtowcN+IHzjtB+BoRU5NP0L876hVwDhR8IHaJ6hOQguN+00aVeu3YZVhKdVS1rM3KQFmZ7Hsrbhl+RKzTlqfVUb5IMoC82w28zT06NYj1LsRuM94vaFawKByxkznnWsJoqtrxUJngfpJp4LKyntE3qKZPeMiFsfaKLTknEmo5afedppaxmIGeUZMkwjEEn5KDOZ5ZYU8byDKZouDnMWJJO8spABB75wJUAZamcADqTMCP7OVrzWEkZyen5SecqGYfLxJ1L0AG4B+pk0CobFs3Yg4PgiJJnRCHQq+oYN0EJVZ+MwiiPz5Oq0uRuU52+cp5T4MmscWPRJmy2QN5JGPcSGZNELZxnOIYGTeR3TlWqbI3B2Of6SLkgeDIUV/CTzKB7ydtTrVzcuR5EU6U24ndM2XAPTEuDlM8vSIg9xGqXAb4ukGgrn6Y3Te2Cjbg+3STSkglk6SoMqkt1B8RnR3lXIs5eVhv2k30d0NsmkyLBSMNnJnHqNSo3KeufJl3EA9dtdlYGCNsdxGEsptsrVm5AR0xnr4hnrJTjTk4+yurUbAqQD+sPIlVumIOVOBkkRp9KqBnQHHffaFGrT1PSc8oIwC3Yxc+0U2Y/GwS1emLkb4yPE7RWaHRubcbZl2vVaWr68uBmNvpzdVWgAdD09hGz0Jwre8eUGK7F9SpRzg4K+IqS1mpavmIKk/MS1NPbp3bfKj8Qi2uATWI4JUZOJkVl4Gtckk2ieq0Ya7KuebAxM+zm9YBhv3HvNeq260OXqzWw+KwjEzNQCLecdF/nGi+8M57ox2qSIWHD5RQR3EVvBJ5u3cRjnZrc5Zc+JVfWy1knPxbSkTls7Q7w/S8nD7Lz/+gKiZNleGI956fTb/AGer2wRkTCFZJ6HrvErlmUi+rpUK69vtFFNa+nYWBAUbbdTFHO5HaP62xQ3pVE+mM/WKhOYiWXfZ5s0l+KIJWBktLEVR8WfYSxqpWaWO4OAN94NiqDQxpUaou1gHNy5A7xK+08x27xi68emFXqBmJNExllLJbVtiVnc7zkkVhDBzklcgywFW67HzI8s7yxsDdkxXn8BBxJoWU83dekqAI6Zlquw22xNHTGuYWDnAw5xzeDGqNRVTUpXBIHKysvXfrM9WAxjbyJNuVhuN+xmbS0bGuy+3TLagsqwrH9XpFgHA/SAY8GFNj1OCOmeh3zLbHDtnGR4MEmDlGXfsilicmMnPiamlswinIJHbsZkmsE5GMeJYtjVn4YOGUNXe4Ps09Q2awp3P5yGnsWs5uQNtsfEWpv6+puD09pMEM2K2yfeLsL8+ZbvY5Zrwx5V2B9+8do01d6rcnPzDZwOpP1nnrk5bdht7R3Rap06HO+wmSq66K1av88WI09fU504J3KE4/L3nNKovX/O9IqMnPeO6K0akrTrK/jbZSp2x2i2sdtCWRQrL4A2+veQSf9Wd03BPkQ81lKqQ7Bx1XMzdfQl+nFiMoUDYD+0U0vEmpt/SAEMerDOJLV6pFtzRZzKwyw5djntHjU4slbq4W1vJoaJafu3ppa432FgA/qZn8RoYMSV2HfzNLQa3TXaUV3IqMvTlEb1Joamt9uU/6jjEnJuMisVXbUuzzNVLK3QCW3j1goVubG/Sbeq4WmpQW6K39Jj/ACyd5jvp7abGD7EdY8ZZIWUuCxjKL9GHbhttbHZScRT0mSnm2wY5plblsGcKcAyJvDMVCjlI5RMj1lj2Ri1FP0YGooIsna1A69ozcPjI7iJs3KSDL7+jyXXiTbJs25x0lLuTsOk6MtuJ0r7RFLLGkngoKnEgVl5GdsSLJ7x9xzuti07LeUQi70ZxlnpGHpEdpqDTE9ofdSe0beW4WZgrMkKz4mn90PiH3UjtN3oOFmcKvaTFRj40reJMaY+Ib0bwyM70j4kvSOJpjSnxJDSt4m8hvAzMFbeICk+JqDSt4kvujHtDeg4WZXpb9JJVw3Qic12v02lc17vYOoGwHzmLbr9Rc+eflHZR0iuwjJqL6N98YawkKB3btEW1umrYlbFY9yqzGex3Pxsxz1yZErzflEdpjm2+keoq+0mmoq+FbTYO+MSnU8dosOBz4I/09J5/0m6zvpHwJPkaZR2WNYZrniembBDNv126S+nU0XryJYGIPTOCZgMjDG2ZwjBz0I8R+V+yLbR67RWV0/EoJPgmd1F11zcxYgds9Z5avUXVA+nYy595o6DjK0nk1lZs/wCcDdfl3jb15Lwvytr6N7hupbT3hrCzqffcGb5rp1lYeoqxHXz9Zi6dK9XQLtOQUPtgy+nnoYFCRvJWJS7R6umudSxLtGhqKhpNFY6bMBy56df5zFTA051BABxhO2T5xN1CdYrB9hj4iDiLa/h9KaYLWcIPiBB7Gc6k4t5PRlXGxKUfSPMWArs3bx/eK8nPZ3jmrQVEKrn8j0ldDMuWFZPuZbd0eTOv8sMseuumkbZfxKAjMM8pxHFAI57Fy35zlrOV+EDHtBMayCErF5RiLtg94y9TOe+TOimuv8RyY6ZySg2KECcjB5c9IQyS2nololgojISHLI7j1FUij0J37v7CMqsmFhuG4kKrp/YyX3b2jYWWKsxzHVURRdN7S9dKCOgjSJL60HeI7GOq4iI0Y8CR1OnWrTu7AYVSeuJrBQBsMTL+0XD24twx9Kj8j5BXfAJHn2mRseRbIYi3Fdny+xX1ersYYBLHOT0H5zq0V8zLZY3Opxy42Pzmx9oOA/4RTSFuLtbkcqoTn3MQco1S016dFUsALt2K+23X5TpyfOyrlGWJeTn3Ct+T0GLkrzMpO48/lIjRMp7lMgcwGRmMUC2qoW0pinmwzBjyO3sfy7H+k0dPcbOT4SjndiTgHrgj5Sc5YL0V7ngSp4e7gZXC53I7f+iX3cLwFFauTy5O3X3nreEcJbUU1qqITao3wfhxg4mhq+A6qmsXamtmCLhFsH4l6d+285Hf2eqtJBLvyfNL9E4sRSAGOwAErXh7vUbMgBcl98kAdfnPS8Up+5XFVZTy4wQCp6nb895ka3UsWKUoKaSAUJb4ht387idMJ7kefqKNjM26nTqwCWMVxliN/pK/u5es2I2eU7A7H6SwtXXy3qobDbowzzA+T0+U5dzOwNdIDD8S1A/IY7SuThaPQfYrUsyWaclDg5A5t8T1g0oXdhn2nk/snwHVW8RXXaqm3T11HKggqS39p78VqwYFfrJTnh9Hs6SMnT+SEatFba36JcDvL24WAh9WxSfAmnpLkSr0zWAe5xmI6jS2XM36XA9gRIWXTR6Omork8tmHquF6RbvU/Gw3C56TO1CK7E4AA6DxPQ26CusENzuD1PLFm0tQ2Sqw+wSShY8nfOutrEUYCU2H4wmR4Et+6ai3cJyjxibK08m66Y/7sRhDcN/Tx+RMrzYOXgb9GCvBtQ4Bbmx9JC3gxQfEcfMz0FlupYYVCPlENTXq3zkp83jRuJT0iSMNuH1g74+sIxZpLyxPqIPyhK8v7OF6d/AyOM8P/a6v3pIcZ4dn/i6vrPCidE6OJHnLXWHvP8a4d+11fWH+NcP7auv6zwwMkP8A3aZxIda6w9yON8P/AGpJYnHOHDrqk/jPCCTUDxDgixvvrf0e+Tj3Cx11a/Qxmv7QcJA31a/un+0+eqBLVVcbkzHpoMda6z4R9Ab7QcKI+HVA/wC0/wBpBOM8OJ/4gfumeIrRD7xympPEz7aKLw1U5HsDxThdi4e4N7FCR/KK54LgIi1BQSf8voT42mNXTV3O8ZSqrsCfmYnEkXWJvMkh/U6Tguq0xqQIjEg8wDDB+W/TP1iP/wAc0qWVfctWXUn4/UBHKcdRt/DBlyhQI1pWTtj5yc4fstCivOUeh+z2jr4ffTfXeHYD40Of4HE9Bx/jGk+0GhFOldAqfjZu3tPL0XgKMED8phfYvVF9JqlZviGoOfoJxSrazg2yiErYtsY4r9ntLdcRVrFrQdLGySD/ANOP6zul+z32e0r+pZYbm6k2PsfkMCO6jlYnf+Ez76l33nRXX15KWUVyfZpGngCYHNpsZzjA2+Uvp1HAK2ZjdpwzbkjqfznmLaKz5ir6ery0qqE/ZCUVHwketfinCwdtVV+8JU3GeFDrrKvmZ4y2lB0P8YlbWufPzjLSL5I2alr0e6t47wpBldXUT7GIW/aLQkn/AOyhHs+J4tkAP9JUyia9DB+WSj9SlX4ij2D8c0JBJvTP/VKTxzRd7kP5tPHlR7/WQOJn2MF4YP6xb/qj2g49oh0uqH5HeRP2h037Qg+c8WfnIGH2UDH9Yt+Eezf7Rab9oSK28d0zdLx9Z5Q5kfpGWkgiMvq9z9I9GeMafP4x+9CebIHicjfbRJ/yl3wiU6JGdzOk81MmJIbSsGSEDcloMmplM7nHXEBsjCke8tVx5igsQfrf90PvFY62DE3KGUjRRgfEZrbHRc/SY41tI6vn8hLU4jpx1YfNTMbRSNuDfqt6ZDfun+kYW7JxnPs238xPO/4ppgMBj8gZXZxrlXloUn/mb+0R4OmOoUfLPVtbyDcgHwYueMabTMPUtGT43njr+Kau7ZrSq+AYtznOcyEo5KP6g1/VHun+1mlCcqB2byBjEzfs9x2rhvrC7m5bbC+R2nmOb3M7z56xONeBJa+xyUuuj6CPtbw9iFZ2Ge5UiMjiWm1S5ouRyewM+ZswgtrocoxU+QcRlXgr/KWe0fQrrABkbeCR/wCIu9pzuG2nj6eK6mrAL8w75HWNJxlTu1BJHfOZaOPZKWrUjbuvz1OfzP8A5ibuD4iJ4tUezL7colbcRpPQt9JRNHPK3I2zjzKmYeYsdbUf12HykTqaj+sfpGyiLmXs0rzK/WQ/rD6zuc78wmZQu46TOEznzzIkwMyBMjCcMBWzuJyGZyBgCSEoNsibT2i7kAwWC7mROoH6ozFjnqTCI5MC1r3bocSsux6sTOQmZYBCEJgBCEIASEJwGEBkdhCEDQhCGZhoQxCcmmZOwM5OEwMbCEIQFCEIQAJ0EjocTkIAWLe69cSwXg7NsYvCNuYDYO20IoDJh2HeMpgXQlXqmE3egK4QhJgEIQmAEIQgAQhCABCEIAEBCEAOjrOwhAcIGEIARhCEBAhCEACEIQAIQhAAhCEACEIQAIQhAAhCEwD/2Q==",
  "https://www.pixelstalk.net/wp-content/uploads/2016/07/Wallpapers-pexels-photo.jpg",
  "https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?cs=srgb&dl=bloom-blooming-blossom-462118.jpg&fm=jpg",
  ];

  void ShowNextImage(){
    setState( () {
      selectedIndex = selectedIndex! + 1;
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold( 
      appBar:AppBar( 
        title: const Text("DISPLAY IMAGE"),       
      ),

      body:Center( 
        child:Column( 
          mainAxisAlignment: MainAxisAlignment.center,

          children: [ 
            Image.network( 
              imageList[selectedIndex!],
              width:300,
              height:300,
            ),
            const SizedBox(
              height:20,
            ),
            ElevatedButton(
              onPressed: ShowNextImage,
              child: const Text("next"),
              ),
            const SizedBox( 
              height: 20,
            ),
            ElevatedButton(
              onPressed:(){ 
                setState( () {
                  selectedIndex =0;
                });
              },
              child: const Text("RESET"),
              ), 
          ],
        ),
      ),
    );
  }
}