
import 'package:flutter/material.dart';

class Assignment extends StatefulWidget{

      const Assignment({super.key});

      @override
      State createState()=> _AssignmentState();
}

class _AssignmentState extends State <Assignment>{
    List<int> countList = [];
    int counter=0;
    void counterD()
    {
      setState(() {
      countList.add(counter++);  
      });
      
    }
    @override
    Widget build(BuildContext context){
      return Scaffold( 
        appBar: AppBar( 
          title: const Text("LISTDEMO"),
          backgroundColor: Colors.blue,
        ),

        body:ListView.builder(
          itemCount: counter,
          itemBuilder: (context,index){ 
            return Container( 
              height: 40,
              width: 300,
              margin: const EdgeInsets.all(10),
              decoration: const BoxDecoration(color:Colors.red),
              child:Text("$index",
              textAlign: TextAlign.center),
            );
          }
      ),
       floatingActionButton: FloatingActionButton( 
        onPressed: counterD,
        child: const Text("+"),
       ),
      );
    }
}