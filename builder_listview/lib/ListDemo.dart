import 'package:flutter/material.dart';

class ListDemo extends StatefulWidget {
  const ListDemo({super.key});

  @override
  State<ListDemo> createState() => _ListDemo();
}

class _ListDemo extends State<ListDemo> {
  List<String> imageList = [
    "https://tse1.mm.bing.net/th?id=OIP.hqgXmy-r13g-63xcvlz6AwHaE7&pid=Api&rs=1&c=1&qlt=95&w=157&h=104",
    "https://tse1.mm.bing.net/th?id=OIP.rvSWtRd_oPRTwDoTCmkP5gHaE8&pid=Api&rs=1&c=1&qlt=95&w=156&h=104",
    "https://tse1.mm.bing.net/th?id=OIP.YAXlTjvtEKchdMVc5laZhwHaE8&pid=Api&rs=1&c=1&qlt=95&w=156&h=104",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("BUILDER DEMO"),
        backgroundColor: Colors.blue,
      ),
      body: ListView.builder(
          itemCount: imageList.length,
          itemBuilder: (context, index) {
            return Container(
              height: 300,
              width: 300,
              margin: const EdgeInsets.all(10),
              child: Image.network(
                fit: BoxFit.contain,
                imageList[index],
              ),
            );
          }),
    );
  }
}
