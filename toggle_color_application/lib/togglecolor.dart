
import 'package:flutter/material.dart';


class togglecolor extends StatefulWidget{
    const togglecolor({super.key});

    @override
    State <togglecolor> createState() => _togglecolorState();

}
class _togglecolorState extends State<togglecolor>{
  
  int _pcount=0;
  int _ncount=0;
 
  Color setColor1(){
        if(_pcount==1){
          return Colors.black;
        }else if(_pcount==2){
          return Colors.green;
        }else if(_pcount==3){
          return Colors.yellow;
        }else{
          _pcount=0;
          return Colors.red;
        }
  }

  Color setColor2(){
        if(_ncount==1){
          return Colors.black;
        }else if(_ncount==2){
          return Colors.green;
        }else if(_ncount==3){
          return Colors.yellow;
        }else{
          _ncount=0;
          return Colors.red;
        }
  }

  @override
  Widget build(BuildContext context){
    return Scaffold( 
      appBar:AppBar( 
        title:const Text("TOGGLECOLORBOX"),
        centerTitle:true,
      ),
      body:Row( 
        mainAxisAlignment:MainAxisAlignment.center,
        crossAxisAlignment:CrossAxisAlignment.center,

        children:[ 
              Column( 
                mainAxisAlignment: MainAxisAlignment.center,
                children: [ 
                  Container( 
                    height:200,
                    width:200,
                    color:setColor1(),
                  ),
                  const SizedBox(
                    height:20,
                  ),
                  ElevatedButton(
                    onPressed:(){
                      setState(() {
                        _pcount++;
                      });
                    },
                     child: const Text("button1")),
                  ],),
                   const SizedBox(width:50),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [ Container(
                    height:200,
                    width:200,
                    color:setColor2(),
                  ),const SizedBox(
                    height:20,
                  ),
                  ElevatedButton(onPressed: (){setState(() {
                    _ncount++;
                  });}, child:const Text("button2")),
                 
                ],
              ),
        ]
      ),
    );
  }
}
