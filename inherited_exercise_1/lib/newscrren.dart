import 'package:flutter/material.dart';
import 'package:inherited_exercise_1/inherited.dart';
import 'package:inherited_exercise_1/skills.dart';

class NewScreen extends StatelessWidget{
  const NewScreen({super.key});

  @override 
  Widget build(BuildContext context){
    SharedData sharedDataobj = SharedData.of(context);
    return Scaffold(
      appBar: AppBar( 
        title:  const Text("newscreen"),
        centerTitle: true,
      ),
      body: Center( 
        child: Column( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Container( 
              height: 50,
              width: 300,
              child:Text("Employee id: ${SharedData.of(context).obj.id}"),
            ),
            const SizedBox( 
              height:20,
            ),
            Container( 
              height: 50,
              width: 300,
              child:Text("Employee name: ${SharedData.of(context).obj.name}"),
            ),
            const SizedBox( 
              height:20,
            ),
            Container( 
              height: 50,
              width: 300,
              child:Text("Employee userName: ${SharedData.of(context).obj.userName}"),
            ),
            const SizedBox( 
              height:20,
            ),

            ElevatedButton(onPressed: (){ 
              Navigator.push(context,MaterialPageRoute(builder: (context)=> SkillsD()),);
            }, child: const Text("SKILLS")),
          ],
        ),
      ),
     );
  }
}
