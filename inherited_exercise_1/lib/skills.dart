

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'inherited.dart';

class SkillsD extends StatefulWidget{
  
      const SkillsD({super.key});

      State createState() => _SkillsDState();
}

class _SkillsDState extends State<SkillsD>{
  TextEditingController skillController  = TextEditingController();

      @override 
      Widget build(BuildContext context){
        return Scaffold( 
          appBar: AppBar( 
            title: const Text("SKILLS"),
          ),
          body: Padding(padding: const EdgeInsets.all(8.0),
          child: Column(children: [ 
            TextField( 
              controller: skillController,
              decoration: InputDecoration(label: Text("skill title")),
            ),

            ElevatedButton(onPressed: (){ 
              SharedData.of(context)
                  .obj
                  .skills
                  .add(skillController.text.trim());
                  setState(() {
                    
                  });
            }, child: const Text("add skills")),

              SizedBox( 
              height: 20,
              child: ListView.builder( 
                itemCount: SharedData.of(context).obj.skills.length,
                itemBuilder: (context , index){ 
                  return Container( 
                    child: Row( children: [ 
                      Text(SharedData.of(context).obj.skills[index])
                    ],),

                  );
                },
              ),
            ),
          ],)
          ),
        );
      }
}