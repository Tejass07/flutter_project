import 'package:flutter/material.dart';
import 'package:inherited_exercise_1/modelclass.dart';
import 'loginpage.dart';
import 'inherited.dart';
void main() {
  runApp( MainApp());
}

class MainApp extends StatelessWidget {
 MainApp({super.key});

  ModelClass obj = ModelClass(id: 10, name:'Tejas', userName: 'Tejas07');

  @override
  Widget build(BuildContext context) {
    return  SharedData(
      obj:obj,
      
      child: MaterialApp(
      home: LoginPage(),
      ),

    );
  }
}
