import 'package:flutter/material.dart';
import 'package:inherited_exercise_1/modelclass.dart';


class SharedData extends InheritedWidget{
  ModelClass obj ;

     SharedData({ 
      super.key,
      required this.obj,
      required super.child,
  });

static SharedData of(BuildContext context){
  return context.dependOnInheritedWidgetOfExactType<SharedData>()!;
}  

@override 
bool updateShouldNotify(SharedData OldWidget){
  return obj != OldWidget.obj;
}

}