import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'inherited.dart';
import 'newscrren.dart';
class LoginPage extends StatefulWidget{
  const LoginPage({super.key});

 State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{
  TextEditingController id = TextEditingController();
  TextEditingController Name = TextEditingController();
  TextEditingController userName = TextEditingController();
  
  @override 
  Widget build(BuildContext context){
    SharedData sharedDataobj = SharedData.of(context);
  
    return Scaffold( 
      appBar: AppBar( 
        title: const Text("Login Page"),
        centerTitle: true,
        backgroundColor: Colors.blue
      ),

      body: Center( 
        child: Column( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 

            TextFormField( 
              controller:id,
              decoration:  InputDecoration( 
                hintText: 'Enter Id',
                label: const Text('Enter ID'),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                prefixIcon:const Icon( 
                  Icons.person,
                ),
              ),
            ),
            const SizedBox( 
              height: 20,
            ),
              TextFormField( 
              controller:Name,
              decoration:  InputDecoration( 
                hintText: 'Enter Name',
                label: const Text('Enter Name'),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                prefixIcon:const Icon( 
                  Icons.abc_rounded,
                ),
              ),
            ),
            const SizedBox( 
              height: 20,
            ),
             TextFormField( 
              controller:userName,
              decoration:  InputDecoration( 
                hintText: 'Enter UserName',
                label: const Text('UserName'),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                prefixIcon:const Icon( 
                  Icons.person,
                ),
              ),
            ),

            const SizedBox( 
              height:20
            ),

            ElevatedButton(onPressed: (){ 
              Navigator.push(context, MaterialPageRoute(builder: (context) => NewScreen()),);
            }, child: const Text("Submit")),


          ],
        ),
      ),
    );

  }

}