class ModelClass {
  final int id;
  final String name;
  final String userName;
  List skills = [];

  ModelClass({ 
    required this.id,
    required this.name,
    required this.userName,
  });
}
