
import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget{
    const Assignment1({super.key});

    @override
    State<Assignment1> createState() => _Assignment1State();
}

class _Assignment1State extends State<Assignment1>{
        int? _count = 0;
       
       void _prinTData(){
        setState( () {
        _count = _count! + 2;
        });
       }
    @override
    Widget build(BuildContext context){
      return Scaffold( 
        appBar: AppBar( 
          title: const Text("TABLE OF 2:"),
        ),

        body: Center(
          child: Column( 
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children :[ 
              const Text("click button to print table of two:"),
            const SizedBox( 
              height:20,
            ),
            Text("$_count"),
            const SizedBox( 
              height:20,
            ),
            ElevatedButton( 
              onPressed:_prinTData,
              child: const Text("print"),
               ),
            ],
          ),
          ),

      );
      }
}