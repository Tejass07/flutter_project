import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const SharedData( 
      data:50,
      child:MaterialApp( 
        home: MainApp(),
      ),

    );
  }
}

class MainApp extends StatefulWidget{
  const MainApp({super.key});
  @override 
  State createState()=> _MainAppState();
}

class _MainAppState extends State<MainApp>{

  @override
  Widget build(BuildContext context){
    SharedData sharedDataObj = SharedData.of(context);
    return Scaffold( 
      appBar: AppBar( 
        title: const Text("inherited Widget"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Column( 
        children: [ 
          Text("${sharedDataObj.data}"),
          const SizedBox(height: 20,)
        ],
      ),
    );
  }
}

class SharedData extends InheritedWidget{
  final int data;
  const SharedData({ 
    super.key,
    required this.data,
    required super.child
  });
  static SharedData of(BuildContext context){ 
    return context.dependOnInheritedWidgetOfExactType<SharedData>()!;
  }
  
  @override
  bool updateShouldNotify(SharedData oldWidget) {
      return data != oldWidget.data; 
  }

}