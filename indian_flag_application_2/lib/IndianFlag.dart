import 'package:flutter/material.dart';

class IndianFlag extends StatefulWidget {
      const IndianFlag({super.key});

      @override
      State <IndianFlag> createState() => _IndianFlagState();      
}
class _IndianFlagState extends State<IndianFlag>{
      int counter=-1;
      @override
      Widget build(BuildContext context){
        return Scaffold (
          appBar:AppBar( 
            title: const Text("INDIAN FLAG ON CLICK"),
            centerTitle:true,
            backgroundColor: Colors.blueAccent,
          ),

          floatingActionButton: FloatingActionButton( 
            onPressed: (){ 
              setState((){ 
                counter++;
            });
          },
          child:const Text("add +"),
          ),

          body:Container( 
            color:Colors.grey,
            child:Row( 
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              
              children: [
                const SizedBox(
                  height:50,
                ), 
                (counter>=0)
                    ?Container( 
                    height: 500,
                    width: 10,
                    color: Colors.black,
                  )
                :Container(),    
              
              Column( 
                children: [ 
                  (counter>=1)
                    ? Container( 
                      height:100,
                      width:500,
                      color:Colors.orange,
                    )
                    : Container(),
                
                
                (counter>=2)
                  ?Container( 
                    color: Colors.white,
                    height:100,
                    width: 500,
                    child:(counter>=3)
                          ?Image.network("https://tse1.mm.bing.net/th?id=OIP.ME43LuxfYNMUt3pTtWnFXAHaJ4&pid=Api&rs=1&c=1&qlt=95&w=79&h=105")
                          :Container(),
                  )
                  :Container(),

                 (counter>=4)
                  ?Container( 
                    color:Colors.green,
                    height:100,
                    width:500,
                  ) 
                  :Container(),
                   ],
              ),
              ],
            ),
          ),
        
        );

      }


}