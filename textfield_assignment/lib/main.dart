import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
      debugShowCheckedModeBanner:false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});



  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 final TextEditingController _namesTextEditingController = TextEditingController();
 final TextEditingController  _phoneTextEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
 
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
     
        title: Text(widget.title, 
        style: const TextStyle( 
        fontStyle:FontStyle.italic,
        backgroundColor: Colors.amber,
        ),
        ),
        centerTitle: true,
     
      ),

      body: Column( 
          children: [ 
            const SizedBox( 
              height: 30,
            ),

             TextField( 
              controller: _namesTextEditingController,
              decoration:InputDecoration( 
                hintText: ("enter company name:"),
              //  border: InputBorder.none,
                  enabledBorder: OutlineInputBorder( 
                    borderRadius:BorderRadius.circular(5.0),
                    borderSide: const BorderSide( 
                      color:Colors.blue,
                    ),
                  ),

              ), 
              cursorColor:Colors.red, 
              keyboardType:TextInputType.name,
              onChanged:(value) { 
                  print("NAME IS =  $value");
              },
            ),

            const SizedBox( 
              height: 30,
            ),
            TextField( 
              controller:_phoneTextEditingController,
              decoration:InputDecoration( 
                hintText: ("enter phoneNO:"),
               // border: InputBorder.none,
                  enabledBorder: OutlineInputBorder( 
                    borderRadius:BorderRadius.circular(5.0),
                    borderSide: const BorderSide( 
                      color:Colors.blue,
                    ),
                  ),

              ), 
              cursorColor:Colors.red, 
              keyboardType:TextInputType.number,
              onSubmitted:(value){ 
                print("Phone no is: $value");
              },


            ),
          ],
      ),
      floatingActionButton: FloatingActionButton( 
        onPressed: (){},
        child: const Text("+"),
      ),
       
    );
  }
}
