
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pikashow',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'PIKA'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text("Pikashow"),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
      body: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        //crossAxisAlignment: CrossAxisAlignment.center,
        

        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 300,
                  width: 300,
                  child: Image.network(
                    "https://tse1.mm.bing.net/th?id=OIP.9RQq34KOh_CypCe4_yk8LwHaEo&pid=Api&P=0&h=180",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 300,
                  width: 300,
                  child: Image.network(
                    "https://tse4.explicit.bing.net/th?id=OIP._YoH2W1XOitmeWGO5HcFwQHaKk&pid=Api&P=0&h=180",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  
                  height: 300,
                  width: 300,
                  color:Colors.black,
                  child: Image.network(
                    "https://tse2.mm.bing.net/th?id=OIP.I1hMdNNd4GsDOX_3HmMaPwHaD4&pid=Api&P=0&h=180",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 300,
                  width: 300,
                  child: Image.network(
                    "https://tse3.mm.bing.net/th?id=OIP.qjUpTOz9BiiZUkioKnv3HAHaLH&pid=Api&P=0&h=180",
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 70,
                width: 70,
                margin: EdgeInsets.all(5),
                child: Image.network(
                    "https://tse3.mm.bing.net/th?id=OIP.fyCCXwkRz9vrY_TsNRq5rgHaHa&pid=Api&P=0&h=180"),
              ),
              Container(
                height: 70,
                width: 70,
                margin: EdgeInsets.all(5),
                child: Image.network(
                    "https://tse2.mm.bing.net/th?id=OIP.uPkEcj-y5SG87n0zkzDX-wHaHa&pid=Api&P=0&h=180"),
              ),
              Container(
                height: 70,
                width: 70,
                margin: EdgeInsets.all(5),
                child: Image.network(
                    "https://tse2.mm.bing.net/th?id=OIP.Cqtx3PyC26XlZzP0luGsRgHaHa&pid=Api&P=0&h=180"),
              ),
              Container(
                height: 70,
                width: 70,
                margin: EdgeInsets.all(5),
                child: Image.network(
                    "https://tse4.mm.bing.net/th?id=OIP.m6moljlv3IS5cwSDi7zicgHaHa&pid=Api&P=0&h=180"),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 70,
                width: 70,
                margin: EdgeInsets.all(5),
                child: Image.network(
                    "https://tse1.mm.bing.net/th?id=OIP.3JArlwme6HGxodCJguTr0QHaHa&pid=Api&rs=1&c=1&qlt=95&w=119&h=119"),
              ),
              Container(
                height: 70,
                width: 70,
                margin: EdgeInsets.all(5),
                child: Image.network(
                    "https://tse1.mm.bing.net/th?id=OIP.hWUeIORTJaC3asRqVx0nngHaHa&pid=Api&rs=1&c=1&qlt=95&w=121&h=121"),
              ),
              Container(
                height: 70,
                width: 70,
                margin: EdgeInsets.all(5),
                child: Image.network(
                    "https://tse1.mm.bing.net/th?id=OIP.GkNOaMKQFP-W2XclDSYMJwHaHa&pid=Api&P=0&h=180"),
              ),
              Container(
                height: 70,
                width: 70,
                margin: EdgeInsets.all(5),
                child: Image.network(
                    "https://tse1.mm.bing.net/th?id=OIP.u14woQvbMi4sZHhNoAXeyQHaHa&pid=Api&rs=1&c=1&qlt=95&w=110&h=110"),
              ),
            ],
          ),
          const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
                SizedBox(
                height: 20,
              ),
              Text(
                "version 82",
                style: TextStyle(color: Colors.white),
              ),
                SizedBox(
                height: 20,
              )
            ],
          ),
          const SizedBox(height: 20,),
          SingleChildScrollView(
          child:
          Row(
            
            children: [
              ElevatedButton(
                onPressed: () {},
                child: const Text("Bollywood",style: TextStyle(color: Color.fromARGB(255, 110, 80, 80)),),
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.black,
                    foregroundColor: Colors.black),
              ),
              const SizedBox(
                width: 20,
              ),
              ElevatedButton(onPressed: () {}, child: const Text("Hollywood",style: TextStyle(color: Colors.grey),),
               style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.black,
                    foregroundColor: Colors.black),),
              const SizedBox(
                width: 20,
              ),
              ElevatedButton(onPressed: () {}, child: const Text("Series",style: TextStyle(color: Colors.grey),),
               style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.black,
                    foregroundColor: Colors.black),),
              const SizedBox(
                width: 20,
              ),
              ElevatedButton(onPressed: () {}, child: const Text("LiveTv",style: TextStyle(color: Colors.grey),),
               style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.black,
                    foregroundColor: Colors.black),),
            ],
          ),
          ),
        ],
      ),
    );
  }
}
