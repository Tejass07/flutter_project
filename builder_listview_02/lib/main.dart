import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

 
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'listview',
      home: const MyHomePage(title: 'LISTVIEW02',),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

 
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 
  @override
  Widget build(BuildContext context) {
    List imageList = [
     ["https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180",
        "https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180",
        "https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180"],
    [

          "https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180",
      "https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180",
      "https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180",
    ],
   [
      "https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180",
      "https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180",
      "https://tse2.mm.bing.net/th?id=OIP.zTKgxyfcz2p08dtJrAmcJAHaFj&pid=Api&P=0&h=180", 
   ],
    ]; 

    List<String> format = ["t20" , "odi" , "test"];  
     return Scaffold(
      appBar: AppBar(
       
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      
        title: Text(widget.title),
        centerTitle: true,
        
      ),
       
      body:
      ListView.separated(
        itemCount: imageList.length,
        //shrinkWrap: true,
        separatorBuilder:(context, index) => const Divider(),
        itemBuilder: (context, index){ 
          return Column( 

              children: [
                Text(format[index]), 
                ListView.builder( 
                  shrinkWrap: true,
                  itemBuilder:(context , ind){ 
                    return Container( 
                      height: 200,
                      width: 200,
                      margin: const EdgeInsets.all(10),
                      child: Image.network(imageList[index][ind]),
                    );
                  },
                  itemCount : (imageList[index].length) 
          ),
              ],
          );
        }, 
        
      ),
    );
  }
}
