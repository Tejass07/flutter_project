import 'package:flutter/material.dart';

class YoutubeDemo extends StatefulWidget {
  const YoutubeDemo({Key? key}) : super(key: key);

  @override
  State createState() => _YoutubeDemoState();
}

class _YoutubeDemoState extends State<YoutubeDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.search)),
          IconButton(onPressed: () {}, icon: Icon(Icons.notifications)),
          IconButton(onPressed: () {}, icon: Icon(Icons.cast)),
        ],
        leading: Image.network(
          "https://tse1.mm.bing.net/th?id=OIP.aIWdGgFKG9Z-P6Bj1BFIowHaHa&pid=Api&rs=1&c=1&qlt=95&w=105&h=105",
          height: 40,
          width: 40,
        ),
        title: const Text(
          "Youtube",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Column(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      const SizedBox(width: 10,),
                      ElevatedButton(onPressed: () {}, child: const Text("All")),
                      const SizedBox(
                        width: 20,
                      ),
                      ElevatedButton(onPressed: () {}, child: const Text("Live")),
                      const SizedBox(
                        width: 20,
                      ),
                      ElevatedButton(onPressed: () {}, child: const Text("Gaming")),
                      const SizedBox(
                        width: 20,
                      ),
                      ElevatedButton(onPressed: () {}, child: const Text("News")),
                      const SizedBox(
                        width: 20,
                      ),
                      ElevatedButton(onPressed: () {}, child: const Text("Actions")),
                      const SizedBox(
                        width: 20,
                      ),
                      ElevatedButton(onPressed: () {}, child: const Text("bgmi")),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  height: 300,
                  width: 415,
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  
                  child: Image.network(
                    "https://tse2.mm.bing.net/th?id=OIP.R7_eg7pvGZDs7XH8hjC3uQHaEK&pid=Api&P=0&h=180",
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                   
                    SizedBox(width:10),
                    Container(
                       
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(30),
                      image: DecorationImage(image: NetworkImage("https://tse2.mm.bing.net/th?id=OIP.R7_eg7pvGZDs7XH8hjC3uQHaEK&pid=Api&P=0&h=180"),
                      fit: BoxFit.cover),
                      ),
                      height: 40,
                      width: 40,
                      
                      
                    ),
                     SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      height: 60,
                      width: 350,
                      
                      child: Text(
                        "Animal Movie || Full animal Movie in Hindi || Animal movie 2023 || Hindi Dubbed",
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                
                Container(
                  height: 300,
                  width: 412,
                  child: Image.network(
                    "https://tse1.mm.bing.net/th?id=OIP.n-kzXMD1jOidjnXhLYTlEAHaEK&pid=Api&P=0&h=180",
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                      const SizedBox(
                        width: 10,
                      ),
                     Container(
                       
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(30),
                      image: DecorationImage(image: NetworkImage("https://tse1.mm.bing.net/th?id=OIP.n-kzXMD1jOidjnXhLYTlEAHaEK&pid=Api&P=0&h=180"),
                      fit: BoxFit.cover),
                      ),
                      height: 40,
                      width: 40,
                      
                      
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      height: 60,
                      width: 350,
                      child: Text(
                        "WAR Movie || Full WAR Movie in Hindi || WAR movie 2024 || Hindi Dubbed",
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 300,
                  width: 412,
                  child: Image.network(
                    "https://tse1.mm.bing.net/th?id=OIP.n-kzXMD1jOidjnXhLYTlEAHaEK&pid=Api&P=0&h=180",
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                  Row(
                  children: [
                      const SizedBox(
                        width: 10,
                      ),
                     Container(
                       
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(30),
                      image: DecorationImage(image: NetworkImage("https://tse1.mm.bing.net/th?id=OIP.n-kzXMD1jOidjnXhLYTlEAHaEK&pid=Api&P=0&h=180"),
                      fit: BoxFit.cover),
                      ),
                      height: 40,
                      width: 40,
                      
                      
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      height: 60,
                      width: 350,
                      child: Text(
                        "WAR Movie || Full WAR Movie in Hindi || WAR movie 2024 || Hindi Dubbed",
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),

                  const SizedBox(height: 20,),
                  const Text("Shorts",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),
                  
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
