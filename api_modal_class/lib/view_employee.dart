import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'employee_model.dart';

class ViewEmployee extends StatefulWidget {
  const ViewEmployee({super.key});

  @override
  State<ViewEmployee> createState() => _ViewEmployeeState();
}

class _ViewEmployeeState extends State<ViewEmployee> {
  List<Data> empData = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("API BINDING"),
        centerTitle: true,
      ),
      body: empData.isEmpty
          ? const Center(child: Text('No data available'))
          : ListView.builder(
              itemCount: empData.length,
              itemBuilder: (context, index) {
                return Row(
                  children: [
                    Text(empData[index].empName ?? 'N/A'),
                    const SizedBox(width: 10),
                    Text("${empData[index].empSal ?? 'N/A'}"),
                  ],
                );
              },
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: getEmployeeData,
        child: const Icon(Icons.add),
      ),
    );
  }

  void getEmployeeData() async {
    Uri url = Uri.parse("https://dummy.restapiexample.com/api/v1/employees");
    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      log(response.body);
      var responseData = json.decode(response.body);

      EmployeeModel empModel = EmployeeModel.fromJson(responseData);

 //     log('Parsed data: ${empModel.data?.length ?? 0} items');

      setState(() {
        empData = empModel.data ?? [];
      });
    } else {
      log('Failed to load data: ${response.statusCode}');
    }
  }
}
