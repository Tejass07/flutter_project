class EmployeeModel {
  String? status;
  List<Data>? data;
  String? message;

  EmployeeModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];

    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }
}

class Data {
  int? empId;
  String? empName;
  int? empAge;
  int? empSal;
  String? profileImage;

  Data.fromJson(Map<String, dynamic> json) {
    empId = json['id'];
    empName = json['employee_name'];
    empAge = json['employee_age'];
    empSal = json['employee_salary'];
    profileImage = json['profile_image'];
  }
}
